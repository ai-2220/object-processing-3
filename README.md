# ДЗ + ПЗ 3 по обработке изображений

## Состав репозитория

В [static](static/) хранятся изображения, видосы и гифки.
В [scripts](scripts/) хранятся код и блокноты

## Задание для новичков
Сделано в очередной раз на C#. Планировалось сделать на хаскеле :wheelchair:, но мы устали, пока мучались с зависимостями...помогите.... (Мы все равно сделаем на хаскеле, но позже :sunglasses:)


#### Запуск программы
```
git clone https://gitlab.com/ai-2220/object-processing-3.git

cd ./scripts/LaserDot

dotnet run
```

### Основной код
```
public class Program
{
    private static string _basePath = $"{Environment.CurrentDirectory.Split("\\bin")[0]}\\Images\\";
    private static List<PointF> laserCenters = new List<PointF>();
    static void Main(string[] args)
    {
        //создаем стринговый массив для удобного хранения названий исходных картиночек
        int arraySize = 8;
        string[] imagesNames = new string[arraySize];
        for (int i = 0; i < arraySize; i++)
        {
            imagesNames[i] = $"img_{i+1}.jpeg";
        }

        //для каждой картиночки вызываем статичный метод по поиску центра лазерной точки
        foreach(var name in imagesNames)
        {
            FindPointCenter(name);
        }

        //считаем дисперсию
        CalculateDispersion();
        CvInvoke.WaitKey(0);
    }

    //а это сам метод
    private static void FindPointCenter(string imageName)
    {
        //считываем изодражение по пути. ImreadModes.Color говорит о том, что изображение цветное
        Mat image = CvInvoke.Imread(_basePath + imageName, ImreadModes.Color);

        //конвертируем bgr в gray
        Mat grayImage = new Mat();
        CvInvoke.CvtColor(image, grayImage, ColorConversion.Bgr2Gray);
        
        //накладываем трешолд для получения бинарного изоюражения
        Mat binaryImage = new Mat();
        CvInvoke.Threshold(grayImage, binaryImage, 250, 255, ThresholdType.Binary);

        //находим контуры. Контуры у нас независимы -> иерархия 0
        VectorOfVectorOfPoint contours = new VectorOfVectorOfPoint();
        CvInvoke.FindContours(binaryImage, contours, null, RetrType.List, ChainApproxMethod.ChainApproxSimple);

        //ищем контур с МАКСИМАЛЬНОЙ площадью
        double maxArea = 0;
        int maxAreaContourIndex = -1;

        for (int i = 0; i < contours.Size; i++)
        {
            double area = CvInvoke.ContourArea(contours[i]);
            if (area > maxArea)
            {
                maxArea = area;
                maxAreaContourIndex = i;
            }
        }

        //получаем минимальный прямоугольник, описывающий наш контур 
        Rectangle boundingBox = CvInvoke.BoundingRectangle(contours[maxAreaContourIndex]);
        
        //поиск координат центра ограничивающего прямоугольника
        PointF center = new PointF(boundingBox.X + boundingBox.Width / 2f, boundingBox.Y + boundingBox.Height / 2f);
        laserCenters.Add(center);

        //выводим координаты центра прямоугольника. Он совпадает с центром лазера
        Console.WriteLine($"Центр лазера: X = {center.X}, Y = {center.Y}");

        // рисуем на начальном изобрадении наш ограничивающий прямоугольнтк
        CvInvoke.Rectangle(image, boundingBox, new MCvScalar(0, 255, 0), 2);

        //а тут рисуем кружок в центре лазера 
        CvInvoke.Circle(image, Point.Round(center), 1, new MCvScalar(0, 0, 255), 2);

        //выводим изображения. Результативное с центром и бинарное.
        // CvInvoke.Imshow($"{imageName}_tresholdet", binaryImage);
        CvInvoke.Imshow($"{imageName}_result", image);
    }


    private static void CalculateDispersion()
    {
        if (laserCenters.Count > 1)
        {
            PointF mean = new PointF(laserCenters.Select(p => p.X).Average(), laserCenters.Select(p => p.Y).Average());

            // Вычисление дисперсии
            float dispersionX = laserCenters.Select(p => (p.X - mean.X) * (p.X - mean.X)).Sum() / (laserCenters.Count - 1);
            float dispersionY = laserCenters.Select(p => (p.Y - mean.Y) * (p.Y - mean.Y)).Sum() / (laserCenters.Count - 1);
            Console.WriteLine($"Дисперсия центра лазера: X = {dispersionX}, Y = {dispersionY}");

            //СКО
            float stdDevX = (float)Math.Sqrt(dispersionX);
            float stdDevY = (float)Math.Sqrt(dispersionY);
            Console.WriteLine($"СКО центра лазера: X = {stdDevX}, Y = {stdDevY}");

        }
        else
        {
            Console.WriteLine("Недостаточно данных для вычисления дисперсии");
        }
           
    }
}
```

### Результаты
<img src="static/result_1.png" width="350px"/>
<img src="static/result_2.png" width="350px"/>

#### Координаты центра
```
Центр лазера: X = 45,5, Y = 203,5
Центр лазера: X = 255, Y = 200
```

#### СКО + дисперсия
```
Дисперсия центра лазера: X = 6016,317, Y = 1,6428572
СКО центра лазера: X = 77,56492, Y = 1,28174
```